import React from 'react';
import configureStore from 'redux-mock-store'; // Smart components
import Game from './Game';
import { shallow } from 'enzyme';
import ToggleOnfOff from '../containers/ToggleOnOff';

const mockStore = configureStore();

const initialState = {
  off: true,
  sequence: [],
  clickable: false,
  lastPushed: {},
  tStepInd: 0,
  index: 0,
  count: 0,
  lock: false,
  counter: '--' 
};

const store = mockStore(initialState);

describe('Game component', () => {
  it('dispatches events to turn on the game', () => {
    const wrapper = shallow(<Game store={store} />);
    expect(store.getActions()).toMatchSnapshot();
  });
});