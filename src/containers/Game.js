import { connect } from 'react-redux';
import { 
  turnOn, 
  turnOff, 
  gameStart, 
  playerSequence, 
  updateLED,
  updateCounter,
  updateGameStatus,
  changePlayer,
  resetPlayerSequence,
  incrementCurrentSeries,
  playerWins,
  playerFails,
} from '../actions';
import Game from '../components/Game';

export default connect(
  (state) => ({
    status: state.gameStatus
  }),
  (dispatch) => ({
    onTurnOn: () => { dispatch( turnOn() ); },
    onTurnOff: () => { dispatch( turnOff() ); },
    onGameStart: () => { dispatch ( gameStart() ); },
    onPlayerSequence: (sequence) => { dispatch (playerSequence(sequence))},
    onUpdateLED: (led) => { dispatch (updateLED(led)) },
    onUpdateCounter: (count) => { dispatch (updateCounter(count)) },
    onUpdateGameStatus: () => { dispatch (updateGameStatus()) },
    onChangePlayer: (player) => { dispatch (changePlayer(player)) },
    onResetPlayerSequence: () => { dispatch(resetPlayerSequence()) },
    onPlayerWins: () => { dispatch(playerWins() ) },
    onPlayerFails: () => { dispatch(playerFails() ) },
    onIncrementCurrentSeries: (currentSeries, count) => { dispatch(incrementCurrentSeries(currentSeries, count) ) }
  })
)(Game);
