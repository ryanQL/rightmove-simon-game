import { connect } from 'react-redux';
import GameLed from '../components/controls/GameLed';
import {updateLED } from '../actions';

export default connect(
  (state) => ({
    status: state.gameStatus
  }),
  (dispatch) => ({
    onUpdateLED: (led) => { dispatch( updateLED(led) ); },
  })
)(GameLed);
