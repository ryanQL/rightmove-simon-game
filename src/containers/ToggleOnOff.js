import { connect } from 'react-redux';
import ToggleOnOff from '../components/controls/ToggleOnOff';
import { turnOff, turnOn } from '../actions';

export default connect(
  (state) => ({
    status: state.gameStatus
  }),
  (dispatch) => ({
    onTurnOn: () => { dispatch( turnOn() ); },
    onTurnOff: () => { dispatch( turnOff() ); }
  })
)(ToggleOnOff);
