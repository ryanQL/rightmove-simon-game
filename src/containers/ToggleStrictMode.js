import { connect } from 'react-redux';
import ToggleStrictMode from '../components/controls/ToggleStrictMode';
import { toggleStrictMode } from '../actions';

export default connect(
  (state) => ({
    status: state.gameStatus
  }),
  (dispatch) => ({
    onToggleStrictMode: () => { dispatch( toggleStrictMode() ); },
  })
)(ToggleStrictMode);
