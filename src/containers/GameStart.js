import { connect } from 'react-redux';
import GameStart from '../components/controls/GameStart';
import { gameStart, updateCounter } from '../actions';

export default connect(
  (state) => ({
    status: state.gameStatus
  }),
  (dispatch) => ({
    onGameStart: () => { dispatch( gameStart() ); },
    onUpdateCounter: (count) => { dispatch ( updateCounter(count) ); }
  })
)(GameStart);
