import React from 'react';
import classNames from 'classnames';

class ToggleOnOff extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  _getClassnames = () => {
    return classNames({
      switch: true,
      'sw-on': this.props.status.off === false
    });
  }

  handleChange = () => {
    if (this.props.status.off) {
      this.props.onTurnOn()
    } else {
      this.props.onTurnOff()
    }
  }

  render() {

    return(
      <div className="rw bot">
        <h3 className="label inline">OFF</h3>
        <div className="sw-slot inline" 
          onClick={this.handleChange}>
          <div className={this._getClassnames()} id="pwr-sw"></div>
        </div>
        <h3 className="label inline">ON</h3>
      </div>
    )
  }
}

export default ToggleOnOff;