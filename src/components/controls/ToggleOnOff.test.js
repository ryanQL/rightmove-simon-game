import React from 'react';
import ToggleOnfOff from '../../containers/ToggleOnOff';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';

const mockStore = configureStore();
const store = mockStore();


describe('GameButton component()', () => {
  it ('renders a button without crashing', () => {
    const mockOnToggleSwitch = jest.fn();
    const wrapper = shallow(<ToggleOnfOff onToggleSwitch={mockOnToggleSwitch} store={store}/>);
    expect(wrapper).toBeTruthy();
  });
})