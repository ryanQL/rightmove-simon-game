import React from 'react';
import classNames from 'classnames';

class GameLed extends React.Component {
  _getClassNames = () => {
    const { off, winner} = this.props.status;
    return classNames({
      count: true,
      'led-off': off && !winner, 
    })
  }

  componentDidMount() {
    this.props.onUpdateLED("--");
  }

  render() {
    return(
      <div className="display inline">
        <h1 className={this._getClassNames()}>{this.props.status.led}</h1>
        <h3 className="label">COUNT</h3>
      </div>
    )
  }
}

export default GameLed;