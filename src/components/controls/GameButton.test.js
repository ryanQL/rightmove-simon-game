import React from 'react';
import GameButton from './GameButton';
import { shallow } from 'enzyme';

describe("GameButton component()", () => {
  
  it ("disables button push" , () => {
    const wrapper = shallow(<GameButton clickable={false}/>);
    expect(wrapper.prop('className')).toContain('unclickable');
  });
  
  it ("enables button push" , () => {
    const wrapper = shallow(<GameButton clickable={true} />);
    expect(wrapper.prop('className')).toContain('clickable');
  });
  
  it ("lights up the button when pressed", ()=> {
    const wrapper = shallow(<GameButton clickable={true} />);
    expect(wrapper.prop('className')).toContain('clickable');
  });
});