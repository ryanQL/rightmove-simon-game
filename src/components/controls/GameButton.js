import React from 'react';
import classNames from 'classnames';
import { soundBoard } from '../../helpers/engine';

class GameButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lightUp: false
    }
    
    this._darken = this._darken.bind(this);
    this.handleButtonPushed = this.handleButtonPushed.bind(this);
  }

  _darken() {
    this.setState({lightUp: false });
  }

  async handleButtonPushed(id, key) {
    if (key) {
      if (soundBoard[key]){
        soundBoard[key].play();
      }
    }
    if (id) {
      if (soundBoard[id]){
        soundBoard[id].play();
      }
    }
    this.setState({lightUp: true });
    setTimeout(this._darken.bind(this), 500);
    this.props.onButtonPushed(id);
  }

  _getClassNames = () => {
    const { 
      position, 
      direction, 
      clickable,
    } = this.props;

    return classNames({
      push: true,
      inline: true,
      't-l': position === 'top'    && direction === 'left',
      't-r': position === 'top'    && direction === 'right',
      'b-l': position === 'bottom' && direction === 'left',
      'b-r': position === 'bottom' && direction === 'right',
      'clickable': clickable,
      'unclickable': !clickable,
      'light': this.state.lightUp,
    }, 'color', this.props.color);
  }

  render() {
    return (
      <div 
        className={ this._getClassNames() } 
        id={this.props.id} 
        key={this.props.id} 
        onClick={ (e) => this.handleButtonPushed(e.target.id) }>
      </div>
    )
  }
}

export default GameButton;