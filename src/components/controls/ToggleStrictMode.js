import React from 'react';
import classNames from 'classnames';

class ToggleStrictMode extends React.Component {
  constructor(props) {
    super(props);  
    this.handleChange = this.handleChange.bind(this);
  }

  _getClassnames = () => {
    const { strictMode, running } = this.props.status;
    return classNames({
      led: true,
      'led-on': strictMode === true,
      'clickable': running,
    });
  }

  handleChange = () => {
    if (!this.props.status.off) {    
      this.props.onToggleStrictMode();
    }
  }

  render() {
    return (
      <div className="btn-box inline">
        <div className="round-btn but" id="mode" onClick={this.handleChange}>
        </div>
        <h3 className="label">STRICT</h3>
        <div className={ this._getClassnames() } id="mode-led"></div>
      </div>
    )
  }
}

export default ToggleStrictMode;