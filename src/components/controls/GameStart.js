import React from 'react';
import classNames from 'classnames';

class GameStart extends React.Component {
  constructor(props) {
    super(props);
    this.handleGameStart = this.handleGameStart.bind(this);
  }
  
  _getClassnames = () => {
    const { running } = this.props.running;
    
    return classNames({
      'round-btn': true,
      'full-red': true,
      'but': true,
      'clickable': !running,
    });
  }

  handleGameStart() {
    if (!this.props.status.off) {
      this.props.onPushed();
    }
  }
    
  render() {
    return (
      <div 
        className="btn-box inline" 
        onClick={ this.handleGameStart } > 
        <div className={this._getClassnames()} id="start">
        </div>
        <h3 className="label">START</h3> 
      </div>
    )
  }
}

export default GameStart;