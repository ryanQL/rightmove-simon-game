import React from 'react';
import GameButton from './controls/GameButton';
import ToggleOnOff from '../containers//ToggleOnOff';
import GameLed from '../containers/GameLed';
import GameStart from '../containers/GameStart';
import ToggleStrictMode from '../containers/ToggleStrictMode';
import gameConstants from '../constants';

class Game extends React.Component {
  
  constructor(props) {
    super(props);

    this._1 = React.createRef();
    this._2 = React.createRef();
    this._3 = React.createRef();
    this._4 = React.createRef();

    this.clickable = this.clickable.bind(this);
    this.handleGameStart = this.handleGameStart.bind(this);
    this.handleButtonPushed = this.handleButtonPushed.bind(this); 
    this.lightUpButton = this.lightUpButton.bind(this);
  }

  checkSeriesCompletion() {
    if(this.props.status.currentSeries.length === this.props.status.series.length) {          
      return true;
    } else {
      return false;
    }
  }

  checkSeriesMatch(currentSeries, sequence ){
    return JSON.stringify(currentSeries) === JSON.stringify(sequence);
  };

  async handleButtonPushed(key) {
    let currentSeries = this.props.status.currentSeries;
    let sequence = this.props.status.sequence;

    if (sequence.length < this.props.status.count){
      await this.props.onPlayerSequence(key);
    }

    sequence = this.props.status.sequence;

    this.gameCheck(sequence, currentSeries);
  } 

  async gameCheck(sequence, currentSeries) {
    if (sequence.length === this.props.status.count) {
      await this.props.onChangePlayer(gameConstants.MACHINE_PLAYER);

      const match = this.checkSeriesMatch(
        currentSeries, 
        sequence
      );

      if (match) {
        if (this.checkSeriesCompletion()) {
          await this.props.onPlayerWins();
        } else {
          const newSeries = this.props.status.series.slice(0, this.props.status.count + 1);
          await this.props.onIncrementCurrentSeries(newSeries, newSeries.length);
          await this.playSequence(this.props.status.currentSeries);  
        }
      } else {
        if (this.props.status.strictMode) {
          await this.props.onPlayerFails();
        } else {
          await this.props.onResetPlayerSequence();
          await this.playSequence(this.props.status.currentSeries);
        }
      }
    }
  }
  
  handleGameStart() {
    if (!this.props.status.running) {
      this.props.onGameStart();
      let count = this.props.status.currentSeries.length;
      this.props.onUpdateCounter(count);
      this.playSequence(this.props.status.currentSeries);
      this.props.onUpdateGameStatus();
    }
  }

  clickable() {
    const { lock, off, player } = this.props.status;
    return (!off && !lock && player === gameConstants.HUMAN_PLAYER)
  }

  buttonToPush(index) {
    switch(index){
      case 1: 
        return this._1;
      case 2:
        return this._2;
      case 3:
        return this._3;
      case 4:
        return this._4;
      default:
        return this._1;
    }
  }

  playSequence(sequence) {
    let index = 0;
    
    this.timer = setInterval( () => {
      this.lightUpButton(sequence[index]);
      index++;
       if (index >= sequence.length ) {
         clearInterval(this.timer);
         this.props.onChangePlayer();
         this.props.onUpdateLED(index);
         this.props.onChangePlayer(gameConstants.HUMAN_PLAYER);
       }
    }, 1200)
  }

  async lightUpButton(index) {
    let buttonRef = this.buttonToPush(index);
    buttonRef.current.handleButtonPushed(buttonRef.current.id, index);
    setTimeout(buttonRef.current._darken.bind(this), 500);
  }
  
  render() {
    return(
      <div className="wrap">
        <div className="wrap-in">
          <div className="rw">
            <GameButton 
              ref={this._1}
              position="top" 
              direction="left" 
              clickable={ this.clickable() }
              id={1}  
              color="green" 
              onButtonPushed={ (e) => { this.handleButtonPushed(e) } } />
            <GameButton
              ref={this._2}
              position="top" 
              direction="right" 
              clickable={ this.clickable() }
              id={2} 
              color="red"
              onButtonPushed={ (e) => { this.handleButtonPushed(e) } } />
          </div>
          <div className="rw">
            <GameButton
              ref={this._3}
              position="bottom" 
              direction="left" 
              clickable={ this.clickable() } 
              id={3} 
              color="yellow"
              onButtonPushed={ (e) => { this.handleButtonPushed(e) } } />
            <GameButton
              ref={this._4}
              position="bottom" 
              direction="right" 
              clickable={ this.clickable() } 
              id={4} 
              color="blue"
              onButtonPushed={ (e) => { this.handleButtonPushed(e) } } />
          </div>
        </div>

        <div className="center">
          <h1 className="brand">Simon<span className="small">®</span></h1>
          <GameLed />
          <GameStart onPushed={this.handleGameStart} running={this.props.status.running}/>
          <ToggleStrictMode />
          <ToggleOnOff />
        </div>
      </div>
    )
  }
};

export default Game;