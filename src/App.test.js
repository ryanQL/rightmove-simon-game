import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import configureStore from './store/configureStore';
import { Provider } from 'react-redux';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const store = configureStore();
  ReactDOM.render(
    (<Provider store={store}>
        <App/>
    </Provider>),
     document.getElementById('root') || document.createElement('div') // for testing purposes
);
  ReactDOM.unmountComponentAtNode(div);
});
