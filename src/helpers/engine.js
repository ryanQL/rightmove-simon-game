export const getRandomIntInclusive = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const getSeries = () => {
  let series = [] 
    for(let i = 0; i < 24; i++) {
      series[i] = getRandomIntInclusive(1,4);
    }
  return series;
};

const sound0 = new Audio("https://s3.us-east-2.amazonaws.com/crawlermachine/machine-sound-1.mp3");
const sound1 = new Audio("https://s3.us-east-2.amazonaws.com/crawlermachine/machine-sound-1.mp3");
const sound2 = new Audio("https://s3.us-east-2.amazonaws.com/crawlermachine/machine-sound-2.mp3"); 
const sound3 = new Audio("https://s3.us-east-2.amazonaws.com/crawlermachine/machine-sound-3.mp3"); 
const sound4 = new Audio("https://s3.us-east-2.amazonaws.com/crawlermachine/machine-sound-4.mp3"); 

export const soundBoard = [
  sound0, 
  sound1, 
  sound2, 
  sound3, 
  sound4
];
