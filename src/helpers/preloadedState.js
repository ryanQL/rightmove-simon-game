import { getSeries } from '../helpers/engine';

const gameSeries = getSeries();

export const getInitialState = () => ({
  series: gameSeries, 
  currentSeries: gameSeries.slice(0,3),
  sequence: [],
  led: '--', 
  count: 0, 
  lock: false, 
  strictMode: false, 
  off: true,
  running: false,
  player: 'machine',
  winner: false,
});