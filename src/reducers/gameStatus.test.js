import gameStatus from './gameStatus';

import {
  TURN_ON,
  TURN_OFF,
  UPDATE_COUNTER,
  TOGGLE_STRICT_MODE,
  PLAYER_SEQUENCE,
  CHANGE_PLAYER,
} from '../actions';

import { getInitialState } from '../helpers/preloadedState';

describe('status', () => {
  describe('INITIAL_STATE', () => {
    test('is correct', () => {
      const action = { type: 'dummy_action' };
      const initialState = getInitialState();
      expect(gameStatus(undefined, action)).toEqual(initialState);
    });
  });

  describe('TURN_ON', () => {
    test('returns the correct state', () => {
      const action = { type: TURN_ON, payload: null };
      const expectedState = getInitialState();
      expect(gameStatus(undefined, action)).toEqual(expectedState);
    });
  });

  describe('TURN_OFF', () => {
    test('returns the correct state', () => {
      const action = { type: TURN_OFF, payload: null };
      const expectedState = getInitialState();
      expect(gameStatus(undefined, action)).toEqual(expectedState);
    });
  });

  describe('UPDATE_COUNTER', () => {
    test('returns the correct state', () => {
      const action = { count: 0, type: UPDATE_COUNTER };
      const expectedState = getInitialState();
      const state = gameStatus(undefined, action);
      expect(state).toEqual(expectedState);
    });
  });

  describe('TOGGLE_STRICT_MODE', () => {
    test('returns the correct state', () => {
      const action = { type: TOGGLE_STRICT_MODE };
      const expectedState = getInitialState();
      const state = gameStatus(undefined, action);
      expect(state).toEqual(expectedState);
    });
  });

  describe('PLAYER_SEQUENCE', () => {
    test('returns the correct state', () => {
      const action = { sequence: 1, type: PLAYER_SEQUENCE };
      const expectedState = getInitialState();
      const state = gameStatus(undefined, action);
      expect(state).toEqual(expectedState);
    });
  });

  describe('CHANGE_PLAYER', () => {
    test('returns the correct state', () => {
      const action = { sequence: 1, type: CHANGE_PLAYER };
      const expectedState = getInitialState();
      const state = gameStatus(undefined, action);
      expect(state).toEqual(expectedState);
    });
  });
});