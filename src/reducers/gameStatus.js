import gameConstants from "../constants";
import { getInitialState } from "../helpers/preloadedState";

export function gameStatus (state = getInitialState(), action) {
  switch (action.type) {
    case gameConstants.TURN_ON:
      return { 
        ...state, 
        off: false, 
        lock: true, 
        count: 0, 
        led: '--',
        running: false,
      };
    case gameConstants.TURN_OFF:
      return { ...state, 
        off: true, 
        lock: true, 
        running: false, 
        led: '--', 
        count: 0,
        sequence: [],
      };
    case gameConstants.PLAYER_WINS:
      return { ...state, 
        off: true, 
        lock: true, 
        running: false, 
        led: 'WINS', 
        count: 0,
        sequence: [],
        winner: true
      };
    case gameConstants.PLAYER_FAILS:
      return { ...state, 
        off: true, 
        lock: true, 
        running: false, 
        led: 'FAIL', 
        count: 0,
        sequence: [],
        winner: true
      };
    case gameConstants.UPDATE_LED:
      return { ...state, 
        led: action.led > 0 ? action.led : state.led }
    case gameConstants.UPDATE_COUNTER:
      return { ...state, count: action.count }
    case gameConstants.UPDATE_GAME_STATUS:
      return { ...state, running: true }
    case gameConstants.GAME_START:
      return { ...state, 
        off: false, 
        lock: true, 
        led: '--',
        count: 0,
        running: true,
        sequence: [],
      }
    case gameConstants.TOGGLE_STRICT_MODE:
      return { ...state, strictMode: !state.strictMode }
    case gameConstants.PLAYER_SEQUENCE:
      return { ...state, 
        sequence: [...state.sequence, parseInt(action.sequence)].filter(Number) }
    case gameConstants.CHANGE_PLAYER:
      if (action.player === gameConstants.HUMAN_PLAYER) {
        return { ...state, 
          lock: action.player === gameConstants.MACHINE_PLAYER, 
          player: action.player,
        } 
      } else {
        return { ...state, 
          lock: action.player === gameConstants.MACHINE_PLAYER, 
          player: action.player,
          led: 'OK'
        } 
      }

    case gameConstants.INCREMENT_CURRENT_SERIES:
      return { ...state,
        currentSeries: action.currentSeries,
        sequence: [],
        count: action.count,
    }
    case gameConstants.RESET_PLAYER_SEQUENCE:
      return { ...state, sequence: [], led: '!--' }
    default:
      return state
  }
};

export default gameStatus;