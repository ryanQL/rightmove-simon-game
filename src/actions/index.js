import gameConstants from '../constants';

export const turnOn = () => ({
  type: gameConstants.TURN_ON,
});

export const turnOff = () => ({
  type: gameConstants.TURN_OFF,
});

export const playerWins = () => ({
  type: gameConstants.PLAYER_WINS,
});

export const playerFails = () => ({
  type: gameConstants.PLAYER_FAILS,
});

export const gameStart = () => ({
  type: gameConstants.GAME_START,
  count: 1,
  led: '--',
  running: true,
})

export const updateLED = (led) => ({
  type: gameConstants.UPDATE_LED,
  led: led,
})

export const updateCounter = (count) => ({
  type: gameConstants.UPDATE_COUNTER,
  count,
})

export const toggleStrictMode = () => ({
  type: gameConstants.TOGGLE_STRICT_MODE,
})

export const playerSequence = (sequence) => ({
  type: gameConstants.PLAYER_SEQUENCE,
  sequence,
})

export const incrementCurrentSeries = (currentSeries, count) => ({
  type: gameConstants.INCREMENT_CURRENT_SERIES,
  currentSeries: currentSeries,
  count: count
})

export const updateGameStatus = () => ({
  type: gameConstants.UPDATE_GAME_STATUS,
  running: true,
})

export const resetPlayerSequence = () => ({
  type: gameConstants.RESET_PLAYER_SEQUENCE,
  sequence: []
})

export const changePlayer = (player) => ({
  type: gameConstants.CHANGE_PLAYER,
  player: player,
})