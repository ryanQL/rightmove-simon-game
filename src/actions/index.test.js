import configureStore from 'redux-mock-store';
import gameConstants from '../constants';
import * as gameActions from './index';

const mockStore = configureStore();
const store = mockStore();

describe('Game actions', () => {
  beforeEach(() => { // Runs before each test in the suite
    store.clearActions();
  });

  describe('turnOn', () => {
    test('Dispatches the correct action and payload', () => {
      const expectedActions = [
        {
          'type': gameConstants.TURN_ON,
        },
      ];
  
      store.dispatch(gameActions.turnOn());
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  
  describe('turnOff', () => {
    test('Dispatches the correct action and payload', () => {
      const expectedActions = [
        {
          'type': gameConstants.TURN_OFF,
        },
      ];
  
      store.dispatch(gameActions.turnOff());
      expect(store.getActions()).toEqual(expectedActions);
    });
  });


  describe('gameStart', () => {
    test('Dispatches the correct action and payload', () => {
      const expectedActions = [
        {
          'led': '--',
          'count': 1,
          'running': true,
          'type': gameConstants.GAME_START,
        },
      ];
  
      store.dispatch(gameActions.gameStart());
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('updateLED', () => {
    test('Dispatches the correct action and payload', () => {
      const expectedActions = [
        {
          'led': '--',
          'type': gameConstants.UPDATE_LED,
        },
      ];
  
      store.dispatch(gameActions.updateLED('--'));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('toggleStrictMode', () => {
    test('Dispatches the correct action and payload', () => {
      const expectedActions = [
        {
          'type': gameConstants.TOGGLE_STRICT_MODE,
        },
      ];
  
      store.dispatch(gameActions.toggleStrictMode());
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('playerSequence', () => {
    test('Dispatches the correct action and payload', () => {
      const expectedActions = [
        {
          'type': gameConstants.PLAYER_SEQUENCE,
          'sequence': 1,
        },
      ];
  
      store.dispatch(gameActions.playerSequence(1));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});

