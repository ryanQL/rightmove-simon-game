const gameConstants = {
  TURN_ON:                  'TURN_ON',
  TURN_OFF:                 'TURN_OFF',
  GAME_START:               'GAME_START',
  GAME_STOP:                'GAME_STOP',
  GAME_RESET:               'GAME_RESET',
  LIGHT_UP:                 'LIGHT_UP',
  TOGGLE_STRICT_MODE:       'TOGGLE_STRICT_MODE',
  UPDATE_COUNTER:           'UPDATE_COUNTER',
  UPDATE_GAME_STATUS:       'UPDATE_GAME_STATUS',
  UPDATE_LED:               'UPDATE_LED',
  PLAYER_SEQUENCE:          'PLAYER_SEQUENCE',
  PLAYER_WINS:              'PLAYER_WINS',
  PLAYER_FAILS:             'PLAYER_FAILS',
  CHANGE_PLAYER:            'CHANGE_PLAYER',
  HUMAN_PLAYER:             'HUMAN',
  MACHINE_PLAYER:           'MACHINE',
  RESET_PLAYER_SEQUENCE:    'RESET_PLAYER_SEQUENCE',
  INCREMENT_CURRENT_SERIES: 'INCREMENT_CURRENT_SERIES',
}

export default gameConstants;